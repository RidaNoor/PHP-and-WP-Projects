<?php
/**
 * Copyright (C) 2014-2017 
 */

class S2sm_Archive_Exception extends Exception {}
class S2sm_Backups_Exception extends Exception {}
class S2sm_Export_Exception extends Exception {}
class S2sm_Http_Exception extends Exception {}
class S2sm_Import_Exception extends Exception {}
class S2sm_Import_Retry_Exception extends Exception {}
class S2sm_Not_Accesible_Exception extends Exception {}
class S2sm_Not_Found_Exception extends Exception {}
class S2sm_Not_Readable_Exception extends Exception {}
class S2sm_Not_Writable_Exception extends Exception {}
class S2sm_Quota_Exceeded_Exception extends Exception {}
class S2sm_Storage_Exception extends Exception {}
