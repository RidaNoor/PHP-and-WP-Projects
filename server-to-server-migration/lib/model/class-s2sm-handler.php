<?php
/**
 * Copyright (C) 2014-2017 
 *
 */

class S2sm_Handler {

	/**
	 * Error handler
	 *
	 * @param  integer $errno   Error level
	 * @param  string  $errstr  Error message
	 * @param  string  $errfile Error file
	 * @param  integer $errline Error line
	 * @return void
	 */
	public static function error( $errno, $errstr, $errfile, $errline ) {
		S2sm_Log::error( array(
			'Number'  => $errno,
			'Message' => $errstr,
			'File'    => $errfile,
			'Line'    => $errline,
		) );
	}
}
