<?php
/**
 * Copyright (C) 2014-2017 
 */

class S2sm_Export_Download {

	public static function execute( $params ) {

		// Set progress
		S2sm_Status::info( __( 'Renaming exported file...', S2SM_PLUGIN_NAME ) );

		// Close achive file
		$archive = new S2sm_Compressor( s2sm_archive_path( $params ) );

		// Append EOF block
		$archive->close( true );

		// Rename archive file
		if ( rename( s2sm_archive_path( $params ), s2sm_download_path( $params ) ) ) {

			// Set archive details
			$link = s2sm_backups_url( $params );
			$size = s2sm_download_size( $params );
			$name = s2sm_site_name();

			$html = "<a href=\"{$link}\" class=\"s2sm-button-green\">".
				"<span>Download Backup</span>".
				"<em>Size: {$size}</em>".
				"</a>".
				"<a href=\"#\" onclick=\"copyToClipboard('{$link}');\" class=\"s2sm-button-green\">".
				"<span>Get URL</span>".
				"</a>";

			// Set progress
			S2sm_Status::download(
			__( $html,S2SM_PLUGIN_NAME)
			);
		}

		return $params;
	}
}
/*sprintf(
                __(
                    '<a href="%s" class="s2sm
-button-green">' .
                    '<span>Download %s</span>' .
                    '<em>Size: %s</em>' .
                    '</a>',
                    S2SM_PLUGIN_NAME
                ),
                $link,
                $name,
                $size
            )*/