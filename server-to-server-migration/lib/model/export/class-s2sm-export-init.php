<?php
/**
 * Copyright (C) 2014-2017 
 */

class S2sm
_Export_Init {

	public static function execute( $params ) {

		// Set archive
		if ( empty( $params['archive'] ) ) {
			$params['archive'] = s2sm
_archive_file();
		}

		// Set storage
		if ( empty( $params['storage'] ) ) {
			$params['storage'] = s2sm
_storage_folder();
		}

		return $params;
	}
}
