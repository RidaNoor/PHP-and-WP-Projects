<?php
/**
 * Copyright (C) 2014-2017 
 *
 */

class S2sm
_Export_Compatibility {

	public static function execute( $params ) {

		// Set progress
		S2sm
_Status::info( __( 'Checking extensions compatibility...', S2SM_PLUGIN_NAME ) );

		// Get messages
		$messages = S2sm
_Compatibility::get( $params );

		// Set messages
		if ( empty( $messages ) ) {
			return $params;
		}

		// Set progress
		S2sm
_Status::error( implode( $messages ) );

		// Manual export
		if ( empty( $params['s2sm
_manual_export'] ) ) {
			if ( function_exists( 'wp_mail' ) ) {

				// Set recipient
				$recipient = get_option( 'admin_email', '' );

				// Set subject
				$subject = __( 'Unable to backup your site', S2SM_PLUGIN_NAME );

				// Set message
				$message = sprintf( __( 'Server-to-Server Migration was unable to backup %s. %s', S2SM_PLUGIN_NAME ), site_url(), implode( $messages ) );

				// Send email
				wp_mail( $recipient, $subject, $message );
			}
		}

		exit;
	}
}
