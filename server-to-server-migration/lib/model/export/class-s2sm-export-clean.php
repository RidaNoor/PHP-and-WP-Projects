<?php
/**
 * Copyright (C) 2014-2017 
 *
 */

class S2sm_Export_Clean {

	public static function execute( $params ) {

		// Remove export log file
		unlink( s2sm_export_path( $params ) );

		// Get storage iterator
		$iterator = new RecursiveIteratorIterator(
			new S2sm_Recursive_Directory_Iterator( s2sm_storage_path( $params ) ),
			RecursiveIteratorIterator::CHILD_FIRST
		);

		// Remove files and directories
		foreach ( $iterator as $item ) {
			if ( $item->isDir() ) {
				rmdir( $item->getPathname() );
			} else {
				unlink( $item->getPathname() );
			}
		}

		// Remove storage path
		rmdir( s2sm
_storage_path( $params ) );

		exit;
	}
}
