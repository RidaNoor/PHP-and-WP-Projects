<?php
/**
 * Copyright (C) 2014-2017 
 *
 */

class S2sm
_Export_Resolve {

	public static function execute( $params ) {

		// Set progress
		S2sm
_Status::info( __( 'Resolving URL address...', S2SM_PLUGIN_NAME ) );

		// HTTP resolve
		S2sm
_Http::resolve( admin_url( 'admin-ajax.php?action=s2sm
_resolve' ) );

		// Set progress
		S2sm
_Status::info( __( 'Done resolving URL address...', S2SM_PLUGIN_NAME ) );

		return $params;
	}
}
