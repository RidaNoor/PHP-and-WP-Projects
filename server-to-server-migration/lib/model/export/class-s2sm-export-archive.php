<?php
/**
 * Copyright (C) 2014-2017 
 *
 */

class S2sm_Export_Archive {
	public static function execute( $params ) {

		// Set progress
		S2sm_Status::info( __( 'Creating an empty archive...', S2SM_PLUGIN_NAME ) );

		// Create empty archive file
		$archive = new S2sm_Compressor( s2sm_archive_path( $params ) );
		$archive->close();

		// Set progress
		S2sm_Status::info( __( 'Done creating an empty archive.', S2SM_PLUGIN_NAME ) );

		return $params;
	}
}
