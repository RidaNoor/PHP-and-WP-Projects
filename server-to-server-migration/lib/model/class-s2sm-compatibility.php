<?php
/**
 * Copyright (C) 2014-2017 
 *
 */

class S2sm_Compatibility {
	public static function get( $params ) {
		$extensions = S2sm_Extensions::get();

		foreach ( $extensions as $extension_name => $extension_data ) {
			if ( ! isset( $params[ $extension_data['short'] ] ) ) {
				unset( $extensions[ $extension_name ] );
			}
		}

		// If no extension is used, update everything that is available
		if ( empty( $extensions ) ) {
			$extensions = S2sm_Extensions::get();
		}

		$messages = array();
		foreach ( $extensions as $extension_name => $extension_data ) {
			$message = S2sm_Compatibility::check( $extension_data );
			if ( ! empty( $message ) ) {
				$messages[] = $message;
			}
		}

		return $messages;
	}

	public static function check( $extension ) {
		if ( $extension['version'] !== 'develop' ) {
			if ( version_compare( $extension['version'], $extension['requires'], '<' ) ) {
				$plugin = get_plugin_data( sprintf( '%s/%s', WP_PLUGIN_DIR, $extension['basename'] ) );
				return sprintf( __(
					'<strong>%s</strong> is not the latest version. ' .
					'You must update the plugin before you can use it. ',
					S2SM_PLUGIN_NAME
				), $plugin['Name'] );
			}
		}
	}
}
