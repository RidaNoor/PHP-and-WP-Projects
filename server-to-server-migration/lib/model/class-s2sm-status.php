<?php
/**
 * Copyright (C) 2014-2017 
 *
 */

class S2sm_Status {

	public static function error( $message, $title = null ) {
		self::log( array( 'type' => 'error', 'message' => $message, 'title' => $title ) );
	}

	public static function info( $message, $title = null ) {
		self::log( array( 'type' => 'info', 'message' => $message, 'title' => $title ) );
	}

	public static function download( $message, $title = null ) {
		self::log( array( 'type' => 'download', 'message' => $message, 'title' => $title ) );
	}

	public static function confirm( $message, $title = null ) {
		self::log( array( 'type' => 'confirm', 'message' => $message, 'title' => $title ) );
	}

	public static function done( $message, $title = null ) {
		self::log( array( 'type' => 'done', 'message' => $message, 'title' => $title ) );
	}

	public static function blogs( $message, $title = null ) {
		self::log( array( 'type' => 'blogs', 'message' => $message, 'title' => $title ) );
	}

	public static function progress( $percent, $title = null ) {
		self::log( array( 'type' => 'progress', 'percent' => $percent, 'title' => $title ) );
	}

	public static function log( $data = array() ) {
		if ( isset( $_REQUEST['s2sm_manual_export'] ) || isset( $_REQUEST['s2sm_manual_import'] ) || isset( $_REQUEST['s2sm_manual_backups'] ) ) {
			update_option( S2SM_STATUS, $data );
		}
	}
}
