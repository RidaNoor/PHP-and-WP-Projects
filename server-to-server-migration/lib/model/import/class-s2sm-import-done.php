<?php
/**
 * Copyright (C) 2014-2017 
 *
 */

class S2sm_Import_Done {

	public static function execute( $params ) {

		// Set shutdown handler
		@register_shutdown_function( 'S2sm_Import_Done::shutdown' );

		// Check multisite.json file
		if ( true === is_file( s2sm_multisite_path( $params ) ) ) {

			// Read multisite.json file
			$handle = s2sm_open( s2sm_multisite_path( $params ), 'r' );

			// Parse multisite.json file
			$multisite = s2sm
_read( $handle, filesize( s2sm
_multisite_path( $params ) ) );
			$multisite = json_decode( $multisite, true );

			// Close handle
			s2sm
_close( $handle );

			// Activate sitewide plugins
			if ( isset( $multisite['Plugins'] ) && ( $plugins = $multisite['Plugins'] ) ) {
				s2sm
_activate_plugins( $plugins );
			}
		} else {

			// Check package.json file
			if ( true === is_file( s2sm
_package_path( $params ) ) ) {

				// Read package.json file
				$handle = s2sm
_open( s2sm
_package_path( $params ), 'r' );

				// Parse package.json file
				$package = s2sm
_read( $handle, filesize( s2sm
_package_path( $params ) ) );
				$package = json_decode( $package, true );

				// Close handle
				s2sm
_close( $handle );

				// Activate plugins
				if ( isset( $package['Plugins'] ) && ( $plugins = $package['Plugins'] ) ) {
					s2sm
_activate_plugins( $plugins );
				}
			}
		}

		// Check blogs.json file
		if ( true === is_file( s2sm
_blogs_path( $params ) ) ) {

			// Read blogs.json file
			$handle = s2sm
_open( s2sm
_blogs_path( $params ), 'r' );

			// Parse blogs.json file
			$blogs = s2sm
_read( $handle, filesize( s2sm
_blogs_path( $params ) ) );
			$blogs = json_decode( $blogs, true );

			// Close handle
			s2sm
_close( $handle );

			// Activate plugins
			foreach ( $blogs as $blog ) {
				if ( isset( $blog['New']['Plugins'] ) && ( $plugins = $blog['New']['Plugins'] ) ) {
					s2sm
_activate_plugins( $plugins );
				}
			}
		}

		return $params;
	}

	public static function shutdown() {

		// Set progress
		S2sm
_Status::done(
			sprintf(
				__(
					'You need to perform two more steps:<br />' .
					'<strong>1. You must save your permalinks structure twice. <a class="s2sm
-no-underline" href="%s" target="_blank">Permalinks Settings</a></strong> <small>(opens a new window)</small><br />' .
					'<strong>2. <a class="s2sm
-no-underline" href="https://wordpress.org/support/view/plugin-reviews/server-to-server-migration?rate=5#postform" target="_blank">Optionally, review the plugin</a>.</strong> <small>(opens a new window)</small>',
					S2SM_PLUGIN_NAME
				),
				admin_url( 'options-permalink.php#submit' )
			),
			__(
				'Your data has been imported successfuly!',
				S2SM_PLUGIN_NAME
			)
		);
	}
}
