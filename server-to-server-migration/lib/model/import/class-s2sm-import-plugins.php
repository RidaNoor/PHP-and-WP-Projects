<?php
/**
 * Copyright (C) 2014-2017 
 *

 */

class S2sm_Import_Plugins {

	public static function execute( $params ) {

		// Set progress
		S2sm
_Status::info( __( 'Activating mu-plugins...', S2SM_PLUGIN_NAME ) );

		// Open the archive file for reading
		$archive = new S2sm
_Extractor( s2sm
_archive_path( $params ) );

		// Unpack mu-plugins files
		$archive->extract_by_files_array( WP_CONTENT_DIR, array( S2SM_MUPLUGINS_NAME ) );

		// Close the archive file
		$archive->close();

		// Set progress
		S2sm_Status::info( __( 'Done activating mu-plugins...', S2SM_PLUGIN_NAME ) );

		return $params;
	}
}
