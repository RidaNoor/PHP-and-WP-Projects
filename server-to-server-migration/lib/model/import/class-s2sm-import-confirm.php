<?php
/**
 * Copyright (C) 2014-2017 

 */

class S2sm_Import_Confirm {

	public static function execute( $params ) {

		// Set progress
		S2sm
_Status::confirm( __(
			'The import process will overwrite your database, media, plugins, and themes. ' .
			'Please ensure that you have a backup of your data before proceeding to the next step.',
			S2SM_PLUGIN_NAME
		) );

		exit;
	}
}
