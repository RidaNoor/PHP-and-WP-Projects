<?php
/**
 * Copyright (C) 2014-2017 
 *
 */

class S2sm_Backups {
	/**
	 * Get all backup files
	 *
	 * @return array
	 */
	public function get_files() {
		$backups = array();

		// Get backup files
		$iterator = new S2sm_Extension_Filter(
			new DirectoryIterator( S2SM_BACKUPS_PATH ),
			array( 'wpress', 'bin' )
		);

		foreach ( $iterator as $item ) {
			try {
				$backups[] = array(
					'filename' => $item->getFilename(),
					'mtime'    => $item->getMTime(),
					'size'     => $item->getSize(),
				);
			} catch ( Exception $e ) {
				$backups[] = array(
					'filename' => $item->getFilename(),
					'mtime'    => null,
					'size'     => s2sm_filesize( $item->getPathname() ),
				);
			}
		}

		// Sort backups modified date
		usort( $backups, array( $this, 'compare' ) );

		return $backups;
	}

	/**
	 * Delete file
	 *
	 * @param  string  $file File name
	 * @return boolean
	 */
	public function delete_file( $file ) {
		if ( empty( $file ) ) {
			throw new S2sm_Backups_Exception( __( 'File name is not specified.', S2SM_PLUGIN_NAME ) );
		} else if ( ! unlink( S2SM_BACKUPS_PATH . DIRECTORY_SEPARATOR . $file ) ) {
			throw new S2sm_Backups_Exception(
				sprintf(
					__( 'Unable to delete <strong>"%s"</strong> file.', S2SM_PLUGIN_NAME ),
					S2SM_BACKUPS_PATH . DIRECTORY_SEPARATOR . $file
				)
			);
		}

		return true;
	}

	/**
	 * Compare backup files by modified time
	 *
	 * @param  array $a File item A
	 * @param  array $b File item B
	 * @return integer
	 */
	public function compare( $a, $b ) {
		if ( $a['mtime'] === $b['mtime'] ) {
			return 0;
		}

		return ( $a['mtime'] > $b['mtime'] ) ? - 1 : 1;
	}
}
