<?php
/**
 * Copyright (C) 2014-2017 
 *

 */

class S2sm_Status_Controller {

	public static function status() {
		echo json_encode( get_option( S2SM_STATUS, array() ) );
		exit;
	}
}
