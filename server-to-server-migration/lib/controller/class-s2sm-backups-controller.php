<?php
/**
 * Copyright (C) 2014-2017 
 *

 */

class S2sm_Backups_Controller {

	public static function index() {
		$model = new S2sm
_Backups;

		S2sm
_Template::render(
			'backups/index',
			array(
				'backups'     => $model->get_files(),
				'username'    => get_option( S2SM_AUTH_USER ),
				'password'    => get_option( S2SM_AUTH_PASSWORD ),
			)
		);
	}

	public static function delete() {
		$response = array( 'errors' => array() );

		// Set archive
		$archive = null;
		if ( isset( $_POST['archive'] ) ) {
			$archive = trim( $_POST['archive'] );
		}

		$model = new S2sm
_Backups;

		try {
			// Delete file
			$model->delete_file( $archive );
		} catch ( Exception $e ) {
			$response['errors'][] = $e->getMessage();
		}

		echo json_encode( $response );
		exit;
	}
}
