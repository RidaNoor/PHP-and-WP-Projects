<div class="error">
	<p>
		<?php
		printf(
			__(
				'Server to Server Migration is not able to create <strong>%s</strong> file. ' .
				'Try to change permissions of the parent folder or send us an email at ' .
				'<a href="mailto:support@#">support@#</a> for assistance.',
				S2SM_PLUGIN_NAME
			),
			S2SM_DIRECTORY_INDEX
		)
		?>
	</p>
</div>
