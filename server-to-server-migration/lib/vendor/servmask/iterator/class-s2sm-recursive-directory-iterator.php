<?php
/**
 * Copyright (C) 2014-2017 
 *
 */

class S2sm_Recursive_Directory_Iterator extends RecursiveDirectoryIterator {

	protected $exclude = array();

	public function __construct( $path ) {
		parent::__construct( $path );

		// Skip current and parent directory
		$this->skipdots();
	}

	public function rewind() {
		parent::rewind();

		// Skip current and parent directory
		$this->skipdots();
	}

	public function next() {
		parent::next();

		// Skip current and parent directory
		$this->skipdots();
	}

	protected function skipdots() {
		while ( $this->isDot() ) {
			parent::next();
		}
	}
}
