<?php
/**
 * Copyright (C) 2014-2017 
 *
 */

class S2sm
_File_Index {

	/**
	 * Create a index.php file
	 *
	 * The method will create index.php file with contents '<?php // silence is golden' without the single quotes
	 * at the path specified by the argument.
	 *
	 * @param  string  $path Path to the index.php file
	 * @return boolean|null
	 */
	public static function create( $path ) {
		$contents = '<?php // silence is golden';
		return S2sm
_File::create( $path, $contents );
	}
}
