<?php
/**
 * Copyright (C) 2014-2017 
 */

class S2sm
_File_Htaccess {

	/**
	 * Create .htaccess file
	 *
	 * The method will create .htaccess file with contents 'AddType application/octet-stream .wpress'
	 *
	 * @param  string  $path Path to the .htaccess file
	 * @return boolean|null
	 */
	public static function create( $path ) {
		$contents = "<IfModule mod_mime.c>\nAddType application/octet-stream .wpress\n</IfModule>";
		return S2sm
_File::create( $path, $contents );
	}
}
