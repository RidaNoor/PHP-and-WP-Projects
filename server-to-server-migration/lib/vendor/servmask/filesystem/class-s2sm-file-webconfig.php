<?php
/**
 * Copyright (C) 2014-2017 
 *
 */

class S2sm
_File_Webconfig {

	/**
	 * Create web.config file
	 *
	 * The method will create web.config file with contents '<mimeMap fileExtension=".wpress" mimeType="application/octet-stream" />'
	 *
	 * @param  string  $path Path to the web.config file
	 * @return boolean|null
	 */
	public static function create( $path ) {
		$contents = "<configuration>\n" .
					"<system.webServer>\n" .
					"<staticContent>\n" .
					"<mimeMap fileExtension=\".wpress\" mimeType=\"application/octet-stream\" />\n" .
					"</staticContent>\n" .
					"</system.webServer>\n" .
					"</configuration>";

		return S2sm
_File::create( $path, $contents );
	}
}
