<?php
/**
 * Copyright (C) 2014-2017 
 *
 */

class S2sm
_File {

	/**
	 * Create a file with contents
	 *
	 * The method will only create the file if it doesn't already exist.
	 *
	 * @param string $path     Path to the file
	 * @param string $contents Contents of the file
	 *
	 * @return boolean|null
	 */
	public static function create( $path, $contents ) {
		if ( ! is_file( $path ) ) {
			$handle = s2sm_open( $path, 'w' );
			if ( false === $handle ) {
				return false;
			}
			s2sm_write( $handle, $contents );
			s2sm_close( $handle );
		}
	}
}
