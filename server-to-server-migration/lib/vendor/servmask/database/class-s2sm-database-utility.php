<?php
/**
 * Copyright (C) 2014-2017 
 *
 */

class S2sm_Database_Utility {

	/**
	 * Replace all occurrences of the search string with the replacement string.
	 * This function is case-sensitive.
	 *
	 * @param  array  $from List of string we're looking to replace.
	 * @param  array  $to   What we want it to be replaced with.
	 * @param  string $data Data to replace.
	 * @return mixed        The original string with all elements replaced as needed.
	 */
	public static function replace_values( $from = array(), $to = array(), $data = '' ) {
		return strtr( $data, array_combine( $from, $to ) );
	}

	/**
	 * Take a serialized array and unserialize it replacing elements as needed and
	 * unserializing any subordinate arrays and performing the replace on those too.
	 * This function is case-sensitive.
	 *
	 * @param  array $from       List of string we're looking to replace.
	 * @param  array $to         What we want it to be replaced with.
	 * @param  mixed $data       Used to pass any subordinate arrays back to in.
	 * @param  bool  $serialized Does the array passed via $data need serializing.
	 * @return mixed             The original array with all elements replaced as needed.
	 */
	public static function replace_serialized_values( $from = array(), $to = array(), $data = '', $serialized = false ) {
		try {

			// Some unserialized data cannot be re-serialized eg. SimpleXMLElements
			if ( is_serialized( $data ) && ( $unserialized = @unserialize( $data ) ) !== false ) {
				$data = self::replace_serialized_values( $from, $to, $unserialized, true );
			} else if ( is_array( $data ) ) {
				$tmp = array();
				foreach ( $data as $key => $value ) {
					$tmp[$key] = self::replace_serialized_values( $from, $to, $value, false );
				}

				$data = $tmp;
				unset( $tmp );
			} elseif ( is_object( $data ) ) {
				$tmp = $data;
				$props = get_object_vars( $data );
				foreach ( $props as $key => $value ) {
					$tmp->$key = self::replace_serialized_values( $from, $to, $value, false );
				}

				$data = $tmp;
				unset( $tmp );
			} else {
				if ( is_string( $data ) ) {
					$data = strtr( $data, array_combine( $from, $to ) );
				}
			}

			if ( $serialized ) {
				return serialize( $data );
			}

		} catch ( Exception $e ) {
			// pass
		}

		return $data;
	}

	/**
	 * Unescape MySQL special characters
	 *
	 * @param  string $data Data to replace.
	 * @return string
	 */
	public static function unescape_mysql( $data ) {
		return str_ireplace(
			array( '\\\\', '\\0', "\\n", "\\r", '\Z', "\'", '\"', '\0' ),
			array( '\\', '\0', "\n", "\r", "\x1a", "'", '"', "\0" ),
			$data
		);
	}
}
