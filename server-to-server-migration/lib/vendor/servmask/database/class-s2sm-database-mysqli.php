<?php
/**
 * Copyright (C) 2014-2017 
 *

 */

class S2sm
_Database_Mysqli extends S2sm
_Database {

	/**
	 * Run MySQL query
	 *
	 * @param  string   $input SQL query
	 * @return resource
	 */
	public function query( $input ) {
		return mysqli_query( $this->wpdb->dbh, $input, MYSQLI_USE_RESULT );
	}

	/**
	 * Escape string input for mysql query
	 *
	 * @param  string $input String to escape
	 * @return string
	 */
	public function quote( $input ) {
		return "'" . mysqli_real_escape_string( $this->wpdb->dbh, $input ) . "'";
	}

	/**
	 * Returns the error code for the most recent function call
	 *
	 * @return int
	 */
	public function errno() {
		return mysqli_errno( $this->wpdb->dbh );
	}

	/**
	 * Returns a string description of the last error
	 *
	 * @return string
	 */
	public function error() {
		return mysqli_error( $this->wpdb->dbh );
	}

	/**
	 * Return server version
	 *
	 * @return string
	 */
	public function version() {
		return mysqli_get_server_info( $this->wpdb->dbh );
	}

	/**
	 * Return the result from MySQL query as associative array
	 *
	 * @param  resource $result MySQL resource
	 * @return array
	 */
	public function fetch_assoc( $result ) {
		return mysqli_fetch_assoc( $result );
	}

	/**
	 * Return the result from MySQL query as row
	 *
	 * @param  resource $result MySQL resource
	 * @return array
	 */
	public function fetch_row( $result ) {
		return mysqli_fetch_row( $result );
	}

	/**
	 * Free MySQL result memory
	 *
	 * @param  resource $result MySQL resource
	 * @return bool
	 */
	public function free_result( $result ) {
		return mysqli_free_result( $result );
	}
}