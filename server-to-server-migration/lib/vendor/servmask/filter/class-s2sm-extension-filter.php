<?php
/**
 * Copyright (C) 2014-2017 
 *
 */

class S2sm
_Extension_Filter extends FilterIterator {

	protected $include = array();

	public function __construct( Iterator $iterator, $include = array() ) {
		parent::__construct( $iterator );

		// Set include filter
		$this->include = $include;
	}

	public function accept() {
		if ( in_array( pathinfo( $this->getInnerIterator()->getFilename(), PATHINFO_EXTENSION ), $this->include ) ) {
			return true;
		}

		return false;
	}
}
